package seleniumjava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearchTest {
	
	private WebDriver driver;
	
	@Before
	public void setUp() throws Exception{
		System.setProperty("webdriver.chrome.driver","./src/test/resources/Chromedriver/Chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");
	}
	
	@Test
	public void testGooglePage() {
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.clear();
		searchBox.sendKeys("Test Automation por selenium");
		searchBox.submit();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		assertEquals("Test Automation por selenium - Buscar con Google",driver.getTitle());
	}
	
	@After
	public void closeTest() {
		
		driver.quit();
	}
	

}
